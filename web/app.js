myApp = angular.module("crud-app", ['ui.router', 'ngCookies']);

myApp
    .constant('URL_BASE', 'https://'+document.domain+'/api/') // production
    // .constant('URL_BASE', 'http://'+document.domain+':5000/api/') //localhost
    .constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    })
    .constant('USER_ROLES', {
        admin: 'admin',
        manager: 'manager',
        developer: 'developer'
    })
    .run(function ($rootScope, LoginService, $state, $location) {
        var routesPermission = ['/home','/first-login','/add-apontamento','/add-projeto','/add-atividade','/listagem','/add-usuario'];

        $rootScope.$on('$stateChangeStart', function (event, next) {
            if (routesPermission.indexOf(next.url) != -1 && !LoginService.isLogged()) {
                $state.go('login');
            }
        });

        // $rootScope.$on('$stateChangeStart', function (event, next) {
        //     if(next.name != 'login'){
        //         var authorizedRoles = next.data.authorizedRoles;
        //         if (!LoginService.isAuthorized(authorizedRoles)) {
        //             event.preventDefault();
        //             if (LoginService.isAuthenticated()) {
        //                 // user is not allowed
        //                 $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        //             } 
        //             else {
        //                 // user is not logged in
        //                 $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        //             }
        //         }
        //     }
        // });
    })
    .config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('app', {
                abstract:true,
                url: '',
                templateUrl: 'app/views/layout/base.html',
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/views/login.html'
            })
            .state('reset-password', {
                url: '/reset-password',
                templateUrl: 'app/views/login.html'
            })
            .state('app.home', {
                url: '/home',
                templateUrl: 'app/views/home.html',
                // data: {
                //     authorizedRoles: [USER_ROLES.admin, USER_ROLES.developer, USER_ROLES.manager]
                // }
            })
            .state('app.add-apontamento', {
                url: '/add-apontamento',
                templateUrl: 'app/views/apontamento.html',
                // data: {
                //     authorizedRoles: [USER_ROLES.admin, USER_ROLES.developer, USER_ROLES.manager]
                // }
            })
            .state('app.add-projeto', {
                url: '/add-projeto',
                templateUrl: 'app/views/projeto.html',
                // data: {
                //     authorizedRoles: [USER_ROLES.admin, USER_ROLES.manager]
                // }
            })
            .state('app.add-atividade', {
                url: '/add-atividade',
                templateUrl: 'app/views/atividade.html',
                // data: {
                //     authorizedRoles: [USER_ROLES.admin, USER_ROLES.manager]
                // }
            })
            .state('app.listagem', {
                url: '/listagem',
                templateUrl: 'app/views/listagem.html',
                // data: {
                //     authorizedRoles: [USER_ROLES.admin, USER_ROLES.developer, USER_ROLES.manager]
                // }
            })
            .state('app.add-usuario', {
                url: '/add-usuario',
                templateUrl: 'app/views/usuario.html',
                // data: {
                //     authorizedRoles: [USER_ROLES.admin]
                // }
            });

});
'use strict';

(function() {
	angular.module('crud-app')
	.service('ListagemService', ListagemService);

	function ListagemService($http, URL_BASE) {
		var service = {
			listar : listar,
			remove : remove
		}

		return service;

		function listar(tipo) {
			return $http.get(URL_BASE + tipo + '/get.php');
		}

		function remove(tipo, id){
			return $http.delete(URL_BASE + tipo + '/delete.php?id=' + id);
		}

	}

})();

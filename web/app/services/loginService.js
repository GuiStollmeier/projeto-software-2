'use strict';

(function() {
	angular.module('crud-app')
	.service('LoginService', LoginService);

	function LoginService($http, URL_BASE, SessionService) {
		var service = {
			login : login,
			logout : logout,
			isLogged : isLogged,
			isAuthorized : isAuthorized
		}

		return service;

		function login(obj) {
			return $http.post(URL_BASE + 'login/post.php', obj).then(function (res) {
				// PRIMEIRO LOGIN DO USUÁRIO:: CONSIGURAR NOVA SENHA::
				if (res.data.resetPassword) {
					return res.data;
				}
				// LOGIN OU SENHA INVALIDOS
				else if (res.data == 1) {
					return 1;
				}
				// LOGIN COM SUCESSO:: INICIA SESSAO::
				else {
					SessionService.set('user', res.data.login);
					return res.data;
				}
      		});
		}

		function logout() {
			SessionService.destroy('user');
		}

		function isLogged() {
			if (SessionService.get('user')) return true;
		}

		function isAuthorized(authorizedRoles) {
			if (!angular.isArray(authorizedRoles)) {
      			authorizedRoles = [authorizedRoles];
    		}

    		// return (isAuthenticated() && authorizedRoles.indexOf(SessionService.userRole) !== -1);
		}

	}

})();

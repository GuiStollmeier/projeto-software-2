'use strict';

(function() {
	angular.module('crud-app')
	.service('StatusApontamentosService', StatusApontamentosService);

	function StatusApontamentosService($http, URL_BASE) {
		var service = {
			get : get
		}

		return service;

		function get() {
			return $http.get(URL_BASE + 'status/get.php');
		}

	}

})();

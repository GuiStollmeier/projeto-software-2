'use strict';

(function() {
	angular.module('crud-app')
	.service('CargoService', CargoService);

	function CargoService($http, URL_BASE) {
		var service = {
			get : get
		}

		return service;

		function get() {
			return $http.get(URL_BASE + 'cargo/get.php');
		}

	}

})();

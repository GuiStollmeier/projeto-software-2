'use strict';

(function() {
	angular.module('crud-app')
	.service('SessionService', SessionService);

	function SessionService($http) {
		var service = {
			get : get,
			set : set,
			destroy : destroy
		}

		return service;

		function set (key, value) {
			return sessionStorage.setItem(key, value);
		}

		function get (key) {
			return sessionStorage.getItem(key);
		}

		function destroy (key) {
			return sessionStorage.removeItem(key);
		}

	}

})();

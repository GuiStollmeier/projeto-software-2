'use strict';

(function() {
	angular.module('crud-app')
	.service('AtividadeService', AtividadeService);

	function AtividadeService($http, URL_BASE) {
		var service = {
			post : post,
			get : get
		}

		return service;

		function post(obj) {
			return $http.post(URL_BASE + 'atividade/post.php', obj);
		}

		function get(id) {
			if(id){
				return $http.get(URL_BASE + 'atividade/get.php?id=' + id);
			}
			else {
				return $http.get(URL_BASE + 'atividade/get.php?');
			}
		}		

	}

})();

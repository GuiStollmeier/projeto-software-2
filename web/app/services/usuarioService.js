'use strict';

(function() {
	angular.module('crud-app')
	.service('UsuarioService', UsuarioService);

	function UsuarioService($http, URL_BASE) {
		var service = {
			post : post,
			get : get
		}

		return service;

		function post(obj) {
			return $http.post(URL_BASE + 'usuario/post.php', obj);
		}

		function get(id, idUsuarios) {
			if(id) {
				return $http.get(URL_BASE + 'usuario/get.php?id=' + id + '&idUsuarios=' + idUsuarios);
			}
			else {
				return $http.get(URL_BASE + 'usuario/get.php');
			}
		}

	}

})();

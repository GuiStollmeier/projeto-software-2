'use strict';

(function() {
	angular.module('crud-app')
	.service('ProjetoService', ProjetoService);

	function ProjetoService($http, URL_BASE) {
		var service = {
			post : post,
			get : get
		}

		return service;

		function post(obj) {
			return $http.post(URL_BASE + 'projeto/post.php', obj);
		}

		function get() {
			return $http.get(URL_BASE + 'projeto/get.php');
		}

	}

})();

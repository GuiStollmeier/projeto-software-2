'use strict';

(function() {
	angular.module('crud-app')
	.service('ApontamentoService', ApontamentoService);

	function ApontamentoService($http, URL_BASE) {
		var service = {
			post : post,
			get : get
		}

		return service;

		function post(obj) {
			return $http.post(URL_BASE + 'apontamento/post.php', obj);
		}

		function get(userID) {
			return $http.get(URL_BASE + 'apontamento/get.php?userID=' + userID);
		}

	}

})();

'use strict';

(function() {
	angular.module('crud-app')
	.service('UserPersistenceService', UserPersistenceService);

	function UserPersistenceService($http, $cookies) {
		var service = {
			setCookieData : setCookieData,
			getCookieData : getCookieData,
			clearCookieData : clearCookieData
		}

		return service;

		function setCookieData(key, value) {
			$cookies.put(key, value);
		}

		function getCookieData(key) {
			return $cookies.get(key);
		}

		function clearCookieData() {
			$cookies.remove('userName');
			$cookies.remove('userCargo');
			$cookies.remove('userLogin');
			$cookies.remove('userID');
		}

	}

})();

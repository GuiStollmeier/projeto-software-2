(function (){
    angular
        .module('crud-app')
        .controller('ListagemController', ListagemController);

    function ListagemController($scope, $http, ProjetoService, AtividadeService, ApontamentoService, UsuarioService) {

        var vm = this;
        vm.listar = listar;
        vm.lastTableShow = '';
        vm.projetos = [];
        vm.atividades = [];
        vm.apontamentos = [];
        vm.buildTable = buildTable;
        vm.data = [];
        vm.columns = [];
        vm.userID = $scope.currentUserID;

        function buildTable(data, tipo, columns){
            if(vm.lastTableShow) {
                $('#' + vm.lastTableShow).DataTable().destroy();
                $('#' + vm.lastTableShow).hide();
            }

            var tableId = 'table_' + tipo;

            $(document).ready(function() {
                $('#'+tableId).DataTable( {
                    dom: 'Bfrtip',
                    data: data,
                    columns: columns,
                    'language': {
                        'lengthMenu': 'Mostrar _MENU_ registros por página',
                        'paginate': {
                            'next':       'Próximo',
                            'previous':   'Voltar'
                        },
                        'search': 'Pesquisar:',
                        'info': 'Mostrando _START_ a _END_ de _TOTAL_ registros',
                        'zeroRecords': 'Nenhum registro encontrado',
                        'emptyTable': 'Sem dados para exibir na tabela'
                    },
                    'columnDefs': [
                        {targets:[-1], className:'center'}
                    ],
                    'buttons': [
                        'excel', 'pdf'
                    ]
                });
            });

            $('#' + tableId).show();
            vm.lastTableShow = tableId;
        }

        function listar(tipo) {
            console.log(tipo);
            if(tipo) {
                vm.data = [];
                vm.columns = [];
                switch(tipo) {
                    case 'projeto':
                        ProjetoService.get().then(
                            function(response) {
                                if(response.data.length > 0) {
                                    for (var i = 0; i < response.data.length; i++) {
                                        var obj = [
                                            response.data[i].nom_projeto,
                                            response.data[i].dsc_projeto
                                        ];
                                        vm.data.push(obj);
                                    }
                                }
                                
                                vm.columns = [
                                    {title: 'Projeto'},
                                    {title: 'Descrição'}
                                ];

                                buildTable(vm.data, tipo, vm.columns);
                            },
                            function(response) {
                                swal('Erro!', 'Erro ao listar projetos!', 'error');
                            }
                        );
                    break;
                    case 'atividade':
                        AtividadeService.get().then(
                            function(response) {
                                if(response.data.length > 0) {
                                    for (var i = 0; i < response.data.length; i++) {
                                        var id_atividade = response.data[i].id_atividade;
                                        var obj = [
                                            response.data[i].nom_atividade,
                                            response.data[i].dsc_atividade,
                                            response.data[i].nom_projeto,
                                            // '<a class="btn btn-primary btn-xs" ng-click="ctrl.excluir(atividade, '+id_atividade+')"> <i class="fa fa-trash"></i> </a>'
                                        ];
                                        vm.data.push(obj);
                                    }
                                }

                                vm.columns = [
                                    {title: 'Atividade'},
                                    {title: 'Descrição'},
                                    {title: 'Projeto'},
                                    // {title: 'Ação'}
                                ];

                                buildTable(vm.data, tipo, vm.columns);
                            },
                            function(response) {
                                swal('Erro!', 'Erro ao listar atividades!', 'error');
                            }
                        );
                    break;
                    case 'apontamento':
                        ApontamentoService.get(vm.userID).then(
                            function(response) {
                                if(response.data.length > 0){
                                    for (var i = 0; i < response.data.length; i++) {
                                        var obj = [
                                            response.data[i].dat_apontamento,
                                            response.data[i].qtd_horas_trabalhadas,
                                            response.data[i].nom_projeto,
                                            response.data[i].nom_atividade
                                        ];
                                        vm.data.push(obj);
                                    }
                                }
                                
                                vm.columns = [
                                    {title: 'Data'},
                                    {title: 'Horas Trabalhadas'},
                                    {title: 'Projeto'},
                                    {title: 'Atividade'}
                                ];

                                buildTable(vm.data, tipo, vm.columns);
                            },
                            function(response) {
                                swal('Erro!', 'Erro ao listar apontamentos!', 'error');
                            }
                        );
                    break;
                    case 'usuario':
                        UsuarioService.get().then(
                            function(response) {
                                if(response.data.length > 0){
                                    for (var i = 0; i < response.data.length; i++) {
                                        var obj = [
                                            response.data[i].nome,
                                            response.data[i].login,
                                            response.data[i].cargo
                                        ];
                                        vm.data.push(obj);
                                    }
                                }
                                
                                vm.columns = [
                                    {title: 'Nome'},
                                    {title: 'Login'},
                                    {title: 'Cargo'}
                                ];

                                buildTable(vm.data, tipo, vm.columns);
                            },
                            function(response) {
                                swal('Erro!', 'Erro ao listar usuários!', 'error');
                            }
                        );
                    break;
                }
            }
        }

    }
})();

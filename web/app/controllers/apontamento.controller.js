(function (){
    angular
        .module('crud-app')
        .controller('ApontamentoController', ApontamentoController);

    function ApontamentoController($scope, $http, ProjetoService, AtividadeService, ApontamentoService) {

        var vm = this;
        vm.apontamento = {};
        vm.apontamento.data = new Date();
        vm.projetos = [];
        vm.atividades = [];
        vm.disabled = true;
        vm.listarProjetos = listarProjetos;
        vm.listaAtividades = listaAtividades;
        vm.salvar = salvar;
        vm.apontamento.userID = $scope.currentUserID;

        listarProjetos();

        function salvar() {
            ApontamentoService.post(vm.apontamento).then(
                function(response) {
                    swal('Sucesso!', 'Registro salvo com sucesso!', 'success');
                    vm.apontamento = {};
                },
                function(response) {
                    swal('Erro!', 'Erro ao salvar!', 'error');
                }
            );
        }

        function listarProjetos() {
            ProjetoService.get().then(
                function(response) {
                    if (response.data) {
                        vm.projetos = response.data;
                    }
                },
                function(response) {
                    swal('Erro!', 'Erro ao listar projetos!', 'error');
                }
            );
        }

        function listaAtividades(projeto) {
            vm.atividades = [];
            vm.disabled = true;

            if (projeto) {
                vm.disabled = false;
                AtividadeService.get(projeto.id).then(
                    function(response) {
                        if (response.data != "false") {
                            vm.atividades = response.data;
                        }
                    },
                    function(response) {
                        swal('Erro!', 'Erro ao listar atividades!', 'error');
                    }
                );
            } 
        }

    }
})();

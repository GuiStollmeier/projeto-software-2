(function (){
    angular
        .module('crud-app')
        .controller('ConsultaController', ConsultaController);

    function ConsultaController($scope, $http) {

        var vm = this;
        vm.dadosSelect = [];
        vm.listarSelect = listarSelect;

        listarSelect();

        function listarSelect(){
            $http({
                method: 'GET',
                url:  '/ferro-velho/api/ferro-velho/get.php'
            }).then(function (response) {
                vm.dadosSelect = response.data.data;
            }, function (response) {

            });
        }

    }
})();

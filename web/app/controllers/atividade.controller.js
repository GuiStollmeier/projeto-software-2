(function (){
    angular
        .module('crud-app')
        .controller('AtividadeController', AtividadeController);

    function AtividadeController($scope, $http, AtividadeService, ProjetoService, UsuarioService) {

        var vm = this;
        vm.cancelar = cancelar;
        vm.atividade = {};
        vm.salvar = salvar;
        vm.projetos = [];
        vm.usuarios = [];
        vm.disabled = true;
        vm.listarProjetos = listarProjetos;
        vm.listarUsuarios = listarUsuarios;

        listarProjetos();

        function listarProjetos() {
            ProjetoService.get().then(
                function(response) {
                    vm.projetos = response.data;
                },
                function(response) {
                    swal('Erro!', 'Erro ao listar projetos!', 'error');
                }
            );
        }

        function listarUsuarios(projeto) {
            vm.usuarios = [];
            vm.disabled = true;

            if (projeto) {
                vm.disabled = false;
                UsuarioService.get(projeto.id, projeto.id_usuario).then(
                    function(response) {
                        if (response.data != "false") {
                            vm.usuarios = response.data;
                        }
                    },
                    function(response) {
                        swal('Erro!', 'Erro ao listar usuários!', 'error');
                    }
                );
            } 
        }

        function salvar(){
            AtividadeService.post(vm.atividade).then(
                function(response) {
                    swal('Sucesso!', 'Registro salvo com sucesso!', 'success');
                    cancelar();
                },
                function(response) {
                    swal('Erro!', 'Erro ao salvar!', 'error');
                }
            );
        }

        function cancelar() {
            vm.atividade = undefined;
        };

    }
})();

(function (){
    angular
        .module('crud-app')
        .controller('ApplicationController', ApplicationController);

    function ApplicationController($scope, $http, UserPersistenceService, $state, LoginService, USER_ROLES) {

        var vm = this;
        $scope.currentUser = UserPersistenceService.getCookieData('userName');
        $scope.currentUserID = UserPersistenceService.getCookieData('userID');
        $scope.currentCargo = UserPersistenceService.getCookieData('userCargo');
        $scope.userRoles = USER_ROLES;
        $scope.isAuthorized = LoginService.isAuthorized;

        $scope.setCurrentUser = function (user) {
            $scope.currentUser = user;
        }

        $scope.logout = function () {
            $scope.currentUser = null;
            $scope.currentCargo = null;
            $scope.currentUserID = null;

            UserPersistenceService.clearCookieData();
            LoginService.logout();
            $state.go('login');
        }

        $scope.closeAlert = function () {
            document.getElementById("alerts").style.display = "none";
        }

    }
})();

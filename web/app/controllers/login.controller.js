(function (){
    angular
        .module('crud-app')
        .controller('LoginController', LoginController);

    function LoginController($scope, $http, $state, LoginService, $rootScope, AUTH_EVENTS, UserPersistenceService, StatusApontamentosService) {

        var vm = this;
        vm.login = login;
        vm.user = {};
        vm.resetPassword = 0;
        vm.newPasswordConfirm = '';
        vm.confirmPassword = 1;
        vm.verificaHorasAtrasadas = verificaHorasAtrasadas;

        if ($state.current.name != 'login') {
            vm.resetPassword = 1;
            vm.user.userID = UserPersistenceService.getCookieData('userID');
            vm.user.login = UserPersistenceService.getCookieData('userLogin');
        }

        function login() {
            if (vm.newPasswordConfirm != '') {
                if (vm.newPasswordConfirm != vm.user.newPassword) {
                    vm.confirmPassword = 0;
                    var div = document.getElementById("div-login-btn");
                    div.classList.add("confirmPassword");
                    return;
                }
            }

            LoginService.login(vm.user).then(
                function(response) {
                    // LOGIN INCORRETO
                    if (response == 1) {
                        swal('Erro!', 'Login ou senha inválidos!', 'error');
                    }
                    // PRIMEIRO LOGIN
                    else if (response.resetPassword) {
                        UserPersistenceService.setCookieData('userLogin', response.login);
                        UserPersistenceService.setCookieData('userID', response.id_usuario);
                        $state.go('reset-password');
                    }
                    // LOGIN COM SUCESSO
                    else {
                        UserPersistenceService.setCookieData('userName', response.nome);
                        UserPersistenceService.setCookieData('userCargo', response.cargo);
                        UserPersistenceService.setCookieData('userID', response.id_usuario);
                        $scope.setCurrentUser(response.nome);
                        $state.go('app.home');

                        if (response.cargo == 'admin' || response.cargo == 'manager') {
                            verificaHorasAtrasadas();
                        }
                    }
                }, 
                function (response) {
                    swal('Erro!', 'Ops! Algo inesperado aconteceu.', 'error');
                }
            );
        }

    

        function verificaHorasAtrasadas () {
            StatusApontamentosService.get().then(
                function(response) {
                    if (response.data.length > 0) {
                        document.getElementById("alerts").style.display = "block";

                        for (var i = response.data.length - 1; i >= 0; i--) {
                            $(".alerts-em-atraso").append(response.data[i]);
                        }
                    }
                },
                function(response) {
                    swal('Erro!', 'Erro ao listar status de apontamentos!', 'error');
                }
            );
        }

    }
})();

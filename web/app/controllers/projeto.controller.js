(function (){
    angular
        .module('crud-app')
        .controller('ProjetoController', ProjetoController);

    function ProjetoController($scope, $http, ProjetoService, UsuarioService) {

        var vm = this;
        vm.cancelar = cancelar;
        vm.salvar = salvar;
        vm.listarUsuarios = listarUsuarios;
        vm.projeto = {};
        vm.usuarios = [];
        vm.checkSelected = checkSelected;

        listarUsuarios();

        function listarUsuarios () {
            UsuarioService.get().then(
                function(response) {
                    vm.usuarios = response.data;
                },
                function(response) {
                    swal('Erro!', 'Erro ao listar usuários!', 'error');
                }
            );
        }

        function checkSelected () {
            var selecionados = [];

            $('.check-usuario:checkbox:checked').each(function () {
                selecionados.push(this.id);
            });

            return selecionados;
        }

        function salvar() {
            vm.projeto.usuarios = checkSelected();

            ProjetoService.post(vm.projeto).then(
                function(response) {
                    swal('Sucesso!', 'Registro salvo com sucesso!', 'success');
                    cancelar();
                },
                function(response) {
                    swal('Erro!', 'Erro ao salvar!', 'error');
                }
            );
        }

        function cancelar() {
            vm.projeto = undefined;
            $('.check-usuario:checkbox').prop('checked', false);
        };
    }
    
})();

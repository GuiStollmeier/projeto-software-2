(function (){
    angular
        .module('crud-app')
        .controller('UsuarioController', UsuarioController);

    function UsuarioController($scope, $http, UsuarioService, CargoService) {

        var vm = this;
        vm.salvar = salvar;
        vm.user = {};
        vm.listarCargos = listarCargos;
        vm.cargos = [];
        vm.user.senha = '123';

        listarCargos();

        function listarCargos() {
            CargoService.get().then(
                function(response) {
                    vm.cargos = response.data;
                },
                function(response) {
                    swal('Erro!', 'Erro ao listar cargos!', 'error');
                }
            );
        }

        function salvar() {
            UsuarioService.post(vm.user).then(
                function(response) {
                    if (response.data == 1) {
                        swal('Erro!', 'Esse login já existe', 'error');
                    }
                    else {
                        vm.user.cargo = vm.user.cargo.dsc_cargo;
                        swal('Sucesso!', 'Registro salvo com sucesso!', 'success');
                        vm.user = undefined;
                    }
                },
                function(response) {
                    swal('Erro!', 'Erro ao salvar!', 'error');
                }
            );
        }

    }
})();

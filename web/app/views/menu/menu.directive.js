(function() {
    angular.module('crud-app')
    .directive('menu', function($http) {
      	return {
        	restrict: 'E',
        	replace: true,
        	templateUrl: "app/views/menu/menu.html"
      	}
    });
})();

angular.module('crud-app').directive('dirFormata', function(){
  return{
    require: "ngModel",
    link: function($scope, $element, $attrs, ctrl){

      var _formatValor = function(valor, campo){
        if(valor){
          if(valor && campo == 'numeral'){
            valor = valor.replace(/[^0-9]+/g, "");
            return valor;
          }
          else if(valor && campo == 'hora'){
            valor = valor.replace(/[^0-9]+/g, "");
            if(valor.length > 2){
              valor = valor.substring(0,2) + ':' + valor.substring(2);
            }
            return valor;
          }
          // FORMATACAO CAMPO  - CNPJ
          if(valor && campo == 'cnpj'){
            valor = valor.replace(/[^0-9]+/g, "");
            if(valor.length > 2){
              valor = valor.substring(0,2) + '.' + valor.substring(2);
            }
            if(valor.length > 6){
              valor = valor.substring(0,6) + '.' + valor.substring(6);
            }
            if(valor.length > 10){
              valor = valor.substring(0,10) + '/' + valor.substring(10);
            }
            if(valor.length > 15){
              valor = valor.substring(0,15) + '-' + valor.substring(15); 
            }
            return valor;
          }
          // FORMATACAO CAMPO  - CPF
          if(campo == 'cpf'){
            valor = valor.replace(/[^0-9]+/g, "");
            if(valor.length > 3){
              valor = valor.substring(0, 3) + '.' + valor.substring(3);
            }
            if(valor.length > 7){
              valor =  valor.substring(0, 7) + '.' + valor.substring(7);
            }
            if(valor.length > 10){
              valor.substring(0, 10) + '.' + valor.substring(10);
            }
            if(valor.length > 11){
              valor = valor.substring(0, 11) + '-' + valor.substring(11);
            }
            return valor;
          }
          // FORMATACAO CAMPO  - TELEFONE
          if(campo == 'telefone'){
            valor = valor.replace(/[^0-9]+/g, "");
            if(valor.length > 0){
              valor = '(' + valor.substring(0);
            }
            if(valor.length > 3){
              valor =  valor.substring(0, 3) + ') ' + valor.substring(3);
            }
            if(valor.length > 10){
              valor = valor.substring(0, 10) + '-' + valor.substring(10);
            }
            return valor;
          }
          // FORMATACAO CAMPO. - CEP
          else if(campo == 'cep'){
            valor = valor.replace(/[^0-9]+/g, "");
            if(valor.length > 5){
              valor = valor.substring(0,5) + "-" + valor.substring(5);
            }
            return valor;
          }
        }
      }

      // CHAMA AS FUNCOES QUANDO O USUARIO DIGITA, PASSANDO O VALOR DO CAMPO $VIEWVALUE E O NOME DO CAMPO ATTRS.NAME
      $element.bind("keyup", function(){
        ctrl.$setViewValue(_formatValor(ctrl.$viewValue, $attrs.name));
        ctrl.$render();
      });

    }
  }
});
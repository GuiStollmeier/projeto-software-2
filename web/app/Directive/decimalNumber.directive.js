angular.module('crud-app').directive('decimalNumber', function(){
  return{
    require: "ngModel",
    link: function($scope, $element, $attrs, ctrl){

      var _formatValor = function(valor){
        if(valor){
          valor = valor.replace(/[^0-9,]+/g, "");

          if(valor.indexOf(',') > 0 && valor.length - 3 > valor.indexOf(',')){
            valor = valor.replace(/[0-9,]+/g, "");
            return valor;
          }
          return valor;
        }
      }

      // CHAMA AS FUNCOES QUANDO O USUARIO DIGITA, PASSANDO O VALOR DO CAMPO $VIEWVALUE E O NOME DO CAMPO ATTRS.NAME
      $element.bind("keyup", function(){
        ctrl.$setViewValue(_formatValor(ctrl.$viewValue));
        ctrl.$render();
      });

    }
  }
});
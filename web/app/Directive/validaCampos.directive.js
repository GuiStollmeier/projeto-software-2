angular.module('crud-app').directive('dirValida', function(){
  return{
    require: "ngModel",
    link: function($scope, $element, $attrs, ctrl){

      var _validaCampo = function(campo, valor){
        if(valor){
          if((campo == 'email' && (valor.indexOf('@') < 0 || valor.indexOf('.') < 0)) || (campo == 'cnpj' && valor.length < 18) || (campo == 'cep' &&  valor.length < 9) || (campo == 'telefone' && valor.length < 14 )){
            document.getElementById(campo).style.border="1px solid red";
          }
          else{
              document.getElementById(campo).style.border="1px solid green";
          }
        }
      }

      // CHAMA AS FUNCOES QUANDO O USUARIO DIGITA, PASSANDO O VALOR DO CAMPO $VIEWVALUE E O NOME DO CAMPO ATTRS.NAME
      $element.bind("keyup", function(){
        _validaCampo($attrs.name, ctrl.$viewValue);
        ctrl.$render();
      });

    }
  }
});
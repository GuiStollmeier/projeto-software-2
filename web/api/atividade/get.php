<?php
require_once('../config/setup.php');
require_once('../config/conexao.php');

$id_projeto = $_GET['id'];
$query = '';

if($id_projeto) {
	$query = 'SELECT * FROM atividade WHERE id_projeto = '.$id_projeto;
}
else {
	$query = '
		SELECT a.id_atividade, a.nom_atividade, a.dsc_atividade, p.nom_projeto 
		FROM atividade a
		INNER JOIN projeto p ON a.id_projeto = p.id;
	';
}

$result = pg_query($dbconn, $query);

$arrRetorno = pg_fetch_all($result);

echo json_encode($arrRetorno);

?>
<?php

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

$dbopts = parse_url(getenv('DATABASE_URL'));


// PRODUCTION::

define("HOST", $dbopts['host']);
define("USER", $dbopts["user"]);
define("PASSWORD", $dbopts["pass"]);
define("DBNAME", ltrim($dbopts['path'],'/'));
define("PORT", $dbopts['port']);
define("URL_BASE", "https://glacial-falls-82830.herokuapp.com/");



// LOCALHOST::

// define("HOST", 'localhost');
// define("USER", 'guilhermestollmeier');
// define("PASSWORD", '');
// define("DBNAME", 'guilhermestollmeier');
// define("PORT", '5432');
// define("URL_BASE", "http://localhost:5000/");

?>

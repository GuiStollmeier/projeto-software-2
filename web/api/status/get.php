<?php
require_once('../config/setup.php');
require_once('../config/conexao.php');

$atraso;

$query = "SELECT at.id_atividade, 
	at.nom_atividade, 
	at.dsc_atividade, 
	u.nome,
	at.horas_estimadas - SUM(ap.qtd_horas_trabalhadas) AS diff_horas, 
	DATE_PART('day', at.dat_encerramento::date) - DATE_PART('day', now()::date) AS diff_dias, 
	DATE_PART('month', at.dat_encerramento::date) - DATE_PART('month', now()::date) AS diff_month 
	FROM atividade at 
	INNER JOIN apontamento ap ON at.id_atividade = ap.id_atividade 
	INNER JOIN usuarios u ON u.id_usuario = at.id_usuario
	GROUP BY at.id_atividade, u.id_usuario;";

$result = pg_query($dbconn, $query);
$arrDados = pg_fetch_all($result);

$divs_alert = [];

foreach ($arrDados as $key => $value) {
	$atraso = false;

	if ($value['diff_horas'] > 0) {
		if (($value['diff_dias'] < 0 && $value['diff_month'] == 0) || ($value['diff_month'] < 0)) {
			$atraso = true;
			$divs_alert[] = '<p style="color: white;padding:0 0 10px 10px">'.$value['nom_atividade'].' ('.$value['nome'].')</p>';
		}
	}
}

echo json_encode($divs_alert);

?>
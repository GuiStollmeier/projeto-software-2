<?php
require_once('../config/setup.php');
require_once('../config/conexao.php');
session_start();

$id_usuario = $_SESSION['userID'];

$query = '
	SELECT 	ap.id_apontamento,
			ap.dat_apontamento,
			ap.qtd_horas_trabalhadas,
			p.nom_projeto,
			at.nom_atividade
	FROM apontamento ap
	INNER JOIN projeto p ON p.id = ap.id_projeto
	INNER JOIN atividade at ON at.id_atividade = ap.id_atividade
	WHERE ap.id_usuario = '.$id_usuario.'
';

$result = pg_query($dbconn, $query);

$arrRetorno = pg_fetch_all($result);

if ($arrRetorno) {
	foreach ($arrRetorno as $key => $value) {
		$arrRetorno[$key]['qtd_horas_trabalhadas'] = str_replace('.', ':', $value['qtd_horas_trabalhadas']);
		$date = date_create($arrRetorno[$key]['dat_apontamento']);
		$arrRetorno[$key]['dat_apontamento'] = date_format($date, 'd/m/Y');
	}

	echo json_encode($arrRetorno);
}

?>